subarch: amd64
target: stage2
version_stamp: desktop-2008.0
rel_type: default
profile: default/linux/amd64/2008.0/desktop
snapshot: 20081014
source_subpath: default/stage1-amd64-2008.0

# Kyron:
portage_confdir: /var/git/clustering-livecd/profiles/hpc-clustering/
pkgcache_path: /var/tmp/catalyst/packages/amd64_desktop
