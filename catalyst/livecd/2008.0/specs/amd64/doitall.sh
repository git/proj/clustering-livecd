cd  /var/git/clustering-livecd/catalyst/livecd/2008.0/specs/amd64/
for I in stage*
do
	mount -t tmpfs -o size=6G,noatime catalystbuild /var/tmp/catalyst/tmp/default
	catalyst -f $I
	# we empty out stuff we don't need anymore ;)
	umount /var/tmp/catalyst/tmp/default
done
mount -t tmpfs -o size=6G,noatime catalystbuild /var/tmp/catalyst/tmp/default
catalyst -f livecd-stage1.spec
catalyst -f livecd-stage2.spec

