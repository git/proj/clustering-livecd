subarch: amd64
version_stamp: minimal-installer-2008.0
target: livecd-stage1
rel_type: default
profile: default/linux/amd64/2008.0/desktop
snapshot: 20081014
source_subpath: default/stage3-amd64-desktop-2008.0

# Kyron:
portage_confdir: /var/git/clustering-livecd/profiles/hpc-clustering/
pkgcache_path: /var/tmp/catalyst/packages/amd64_minimal
portage_overlay: /var/git/clustering-livecd/overlay/ /var/svnroot/livecd-tools /usr/local/portage/layman/sunrise /usr/local/portage/layman/science
### end

# Kyron: added 
# openmp (might as well since we have gcc-4.3.1
# autoconfigure authmaster for ldap-auth
# tftp for dnsmasq pulled in by beowulf-cluster
# -minimal (for openldap to be used as auth server)
# -fam to fix circular dependancy preventing the build
# nfsexport, nfs, hinotify are for aufs (in stage2)
# pbs, server, cpusets for torque
livecd/use:
	-*
	deprecated
	fbcon
	ipv6
	livecd
	loop-aes
	lvm1
	ncurses
	nls
	nptl
	nptlonly
	pam
	readline
	socks5
	ssl
	unicode
	branding
	livecd
	loop-aes
	socks5
	ssl
	unicode
# Clustering:
	ldap
	openmp
	authmaster
	tftp
	-minimal
	-fam
	nfsexport
	hinotify
	nfs
	nonfsv4
	beep
	resolvconf
	pbs
	server

livecd/packages:
	app-accessibility/brltty
	app-admin/hddtemp
	app-admin/passook
	app-admin/pwgen
	app-admin/syslog-ng
	app-arch/unzip
	app-editors/vim
	app-misc/screen
	app-misc/vlock
	app-portage/gentoolkit
	app-portage/mirrorselect
	app-portage/ufed
	app-text/wgetpaste
	media-gfx/fbgrab
	net-analyzer/tcptraceroute
	dev-util/ccache
	dev-util/cvs
	dev-util/git
	dev-util/subversion
	net-analyzer/traceroute
	net-dialup/mingetty
	net-dialup/minicom
	net-dialup/pptpclient
	net-dialup/rp-pppoe
	net-firewall/iptables
	net-fs/mount-cifs
	net-fs/nfs-utils
	net-irc/irssi
	net-misc/bridge-utils
	net-misc/dhcpcd
	net-misc/iputils
	net-misc/ntp
	net-misc/rdate
	net-misc/vconfig
	net-misc/whois
	net-proxy/dante
	net-proxy/ntlmaps
	net-proxy/tsocks
	net-wireless/ipw2100-firmware
	net-wireless/ipw2200-firmware
	net-wireless/iwl3945-ucode
	net-wireless/iwl4965-ucode
	net-wireless/prism54-firmware
	net-wireless/wireless-tools
	net-wireless/wpa_supplicant
	net-wireless/zd1201-firmware
	net-wireless/zd1211-firmware
	sys-apps/apmd
	sys-apps/eject
	sys-apps/ethtool
	sys-apps/fxload
	sys-apps/gli
	sys-apps/hdparm
	sys-apps/hwsetup
	sys-apps/iproute2
	sys-apps/memtester
	sys-apps/netplug
	sys-apps/parted
	sys-apps/sdparm
	sys-apps/sg3_utils
	sys-apps/slocate
	sys-apps/smartmontools
	sys-block/aoetools
	sys-block/disktype
	sys-block/qla-fc-firmware
	sys-boot/grub
	sys-boot/syslinux
	sys-devel/distcc
	sys-fs/cryptsetup
	sys-fs/dmraid
	sys-fs/dosfstools
	sys-fs/e2fsprogs
	sys-fs/evms
	sys-fs/hfsplusutils
	sys-fs/hfsutils
	sys-fs/jfsutils
	sys-fs/lsscsi
	sys-fs/lvm2
	sys-fs/mac-fdisk
	sys-fs/mdadm
	sys-fs/ntfsprogs
	sys-fs/reiserfsprogs
	sys-fs/xfsprogs
	sys-kernel/genkernel
	sys-libs/gpm
	sys-power/acpid
	sys-process/htop
	sys-process/vixie-cron
	www-client/links
## Kyron:
# explicitly adding net-nds/openldap so it
# gets rebuilt with -minimal (can't do that in
# stage3 at the moment.
	net-nds/openldap
	app-portage/portage-utils
	net-nds/ldap-auth
# Added growmacs as per Alexey Shvetsov's request ;)
# from: Bug 193532	
#	sci-chemistry/gromacs
# man needs this:
	app-arch/lzma-utils
# We do unusual things to resolv.conf:
    net-dns/openresolv
	net-dns/bind-tools
# Clustering + HPC specifics	
	sys-cluster/beowulf-head
	app-shells/pdsh
# empi is a meta-ebuild with tools to 
# implement more than one ebuild which means
# we'll have to call empi stuff from within fsscript.
#	sys-cluster/empi
#	sys-cluster/lam-mpi
	sys-cluster/openmpi
	
	app-admin/eselect-cblas
	app-admin/eselect-blas
	app-admin/eselect-lapack
# Misc utilities
	net-misc/wol
	net-misc/ethercard-diag
	sys-apps/dstat
	dev-util/strace
	dev-util/ltrace
