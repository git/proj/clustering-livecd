subarch: amd64
version_stamp: minimal-installer-2008.0
target: livecd-stage2
rel_type: default
profile: default/linux/amd64/2008.0/desktop
snapshot: 20081014
source_subpath: default/livecd-stage1-amd64-minimal-installer-2008.0

# Kyron:
portage_confdir: /var/git/clustering-livecd/profiles/hpc-clustering/
pkgcache_path: /var/tmp/catalyst/packages/amd64_minimal
portage_overlay: /var/git/clustering-livecd/overlay/ /var/svnroot/livecd-tools /usr/local/portage/layman/sunrise /usr/local/portage/layman/science

livecd/iso: /tmp/livecd-amd64-minimal-2008.0.iso
livecd/volid: x86_64 Gentoo Clustering LiveCD
## Note: Can't have motd _with_ gentoo-release-livecd
#livecd/motd: /var/git/clustering-livecd/catalyst/livecd/2008.0/misc/clusterlivecd.motd.txt
#livecd/type: generic-livecd
livecd/type: gentoo-release-livecd
livecd/bootargs: nox
### end

livecd/cdtar: /usr/lib/catalyst/livecd/cdtar/isolinux-elilo-memtest86+-cdtar.tar.bz2
livecd/fsscript: /var/git/clustering-livecd/catalyst/livecd/2008.0/scripts/livecd.sh
livecd/fstype: squashfs
#livecd/gk_mainargs: --lvm --dmraid --evms --mdadm --makeopts=-j5 --no-splash
livecd/gk_mainargs: --makeopts=-j5 --no-splash
#livecd/xsession: xfce
#livecd/xdm: gdm

livecd/overlay: /var/svnroot/releng/trunk/releases/2008.0/overlays/common/overlay/livecd
livecd/root_overlay: /var/git/clustering-livecd/catalyst/livecd/2008.0/overlay/

boot/kernel: gentoo
boot/kernel/gentoo/sources: gentoo-sources

boot/kernel/gentoo/config: /var/git/clustering-livecd/catalyst/livecd/2008.0/kconfig/livecd-2.6.25.config
boot/kernel/gentoo/use:
	-*
	atm
	deprecated
	fbcon
	fbcondecor
	ipv6
	livecd
	loop-aes
	lvm1
	mng
	ncurses
	nls
	nptl
	nptlonly
	pam
	png
	readline
	socks5
	ssl
	truetype
	unicode
	usb
#Clustering:
	nfsexport
	nfs
	hinotify
	nonfsv4
	beep
	resolvconf
	pbs
	server
# Kernel support is built in but this feature is quite experimental in Torque
#	cpusets

boot/kernel/gentoo/packages:
	net-dialup/globespan-adsl
	net-misc/br2684ctl
	net-wireless/hostap-utils
	sys-apps/pcmciautils
	sys-fs/ntfs3g
## kyron: let the fun begin, we need a unionfs-ish FS to be able to configure the clustering stuff on the liveCD
	sys-fs/aufs
## Torque is rebuilt due to kernel dependencies (if cpusets is enabled)
	sys-cluster/torque

livecd/rcadd:
	ntpd|default
	ntp-client|default

livecd/empty:
	/var/tmp
	/var/empty
	/var/run
	/var/state
	/var/cache/edb/dep
	/tmp
	/usr/portage
	/usr/src
	/root/.ccache
	/usr/share/genkernel/pkg/x86/cpio

livecd/rm:
	/etc/*-
	/etc/*.old
	/root/.viminfo
	/var/log/*.log
	/usr/share/genkernel/pkg/x86/*.bz2
