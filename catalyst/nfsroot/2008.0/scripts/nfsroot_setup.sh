#!/bin/bash
# (C) Eric Thibodeau GPL v2

eecho(){
	echo "====================================="
	echo "$*"
	echo "====================================="
}

unionfs_prep()
{
	eecho "=adding unionfs required dirs========"
	mkdir -p /mnt/rw_mounts/
}

set_runlevel()
{
	ln -s /etc/runlevels/default /etc/runlevels/unionfs
}

# We do this often in scripts to config files, change OPTION=something to OPTION=other
# in: 
#	$1: OPTION=other
#	$2: /path/to/file.conf
#	$3: (Optional) assignment token, defaults to =
# out:
#	Changes are made inline

change_opt() {
	SEP=${3:-=}
	KEY=${1#*${SEP}}
	VAL=${1%${SEP}*}
	# Replace old value with the new one ;)
	sed -e"s:${VAL}${SEP}.*:$1:" -i $2
}

openrc_diskless_setup() {
	eecho "=Setting up default RC configs======="

	# /etc/rc.conf
	for I in 'rc_parallel="yes"' 'rc_depend_strict="NO"' 'rc_tty_number=2'
	do
		change_opt $I /etc/rc.conf
	done

	# /etc/conf.d/bootmisc
	change_opt 'wipe_tmp="NO"' /etc/conf.d/bootmisc

	# Using EOF is nice but the vars get interpreted
	# /etc/conf.d/net
	cat > /etc/conf.d/net <<-'EOF'
	preup() {
		local c=0 i
	
		for i in /sys/devices/system/cpu/cpu[0-9]*; do
			c=$((${c} + 1))
		done
	
		dhcpcd_eth0="--persistent --userclass=\"${c}\""
	}
	EOF
# The above is an alternative which doesn't use wc, thus no superficial process spawning.
#	echo 'dhcpcd_eth0="--persistent --userclass=\"$(ls -1d /sys/devices/system/cpu/cpu[0-9]* | /usr/bin/wc -l)\""' >> /etc/conf.d/net
	
	# Set clock to localtime as default
	
	change_opt 'clock="local"'       /etc/conf.d/hwclock
	change_opt 'clock_systohc="YES"' /etc/conf.d/hwclock

	# We don't do this anymore since it puts files into /lib64/rc/init.d/
	# and that causes error messages on reboot:
	#echo "Pre-generating dependencies..."
	#/lib/rc/bin/rc-depend -u
	# Instead, we actually clean out that folder:
	rm -Rf /lib64/rc/init.d/*
	# The following is only useful if some freak grabs the livecd and is in a TZ 
	# making the /etc files dated in the future (my system is UTC-5
	echo "Moving dates to the past so the Cache isn't always regenerated"
	find /etc/ -exec touch --date=yesterday {} \;
	
}

setup_ssh() {
	# Pre-generating sshd keys can be the source of philosophical debates:
    eecho "=Pre-generating sshd keys============"
	. /etc/init.d/sshd
	gen_keys	
#	ln -s /etc/runlevels/default /etc/runlevels/unionfs
}

dash_is_sh() {
	eecho "=Replacing sh with dash=============="
	rm /bin/sh
	ln -s /bin/dash /bin/sh
}

clear_resolv.conf(){
	# we don't want the buildhost's resolv.conf to lie around and confuse things
	eecho "=Clearing our resolv.conf============"
	echo "" > /etc/resolv.conf
}

setup_ntp-client(){
	eecho "=Configuring ntp-client=============="
	sed -ie 's:NTPCLIENT_OPTS=.*:NTPCLIENT_OPTS="-s -b -u master.gentoo.local":' /etc/conf.d/ntp-client
}

dash_is_sh
unionfs_prep
openrc_diskless_setup
# this one is a hack since catalyst doesn't do it for the moment for some reason
setup_ssh

# TEMPORARY for testing:
eecho "=Changing root password=============="
echo root:test | chpasswd

exit 0
