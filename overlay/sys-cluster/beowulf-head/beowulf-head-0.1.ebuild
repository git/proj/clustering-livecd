# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="This is a meta ebuild that pulls in the packages and some config scripts/files for building the Gentoo Clustering Master node"
HOMEPAGE="http://git.overlays.gentoo.org/gitweb/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="livecd autofs nonfsv4 beep pbs"

DEPEND=""
RDEPEND="net-nds/ldap-auth
	sys-boot/syslinux
	net-dns/dnsmasq
	net-misc/sipcalc
	net-fs/nfs-utils
	sys-cluster/c3
	net-dns/openresolv
	beep? (app-misc/beep)
	pbs? (sys-cluster/torque)"

CONFPATH="/etc/gentoo"
CONFIG_FILE="cluster.conf"
MODULESPATH="/usr/libexec/clustering/"
DNSMASQSCRIPT="/usr/sbin/node-manager"

inherit confutils

get_ip_config()
{
	CLUSTER_ETH_IP="$(sipcalc ${CLUSTER_ETH} | egrep 'Host address\W*-' | cut -d- -f2)"
	IPSTART="${CLUSTER_ETH_IP%.*}.$(( ${CLUSTER_ETH_IP##*.} +1 ))"
	IPSTOP="   $(sipcalc ${CLUSTER_ETH} | egrep 'Usable range\W*-'    | cut -d- -f3)"
	SUBNET="   $(sipcalc ${CLUSTER_ETH} | egrep 'Network mask\W*-'    | cut -d- -f2)"
	NETADDR="  $(sipcalc ${CLUSTER_ETH} | egrep 'Network address'     | cut -d- -f2)"
	NODECOUNT="$(sipcalc ${CLUSTER_ETH} | egrep 'Addresses in network'| cut -d- -f2)"

	# We have to clean up the vars from spaces:
	CLUSTER_ETH_IP=${CLUSTER_ETH_IP// }
	IPSTART=${IPSTART// }
	IPSTOP=${IPSTOP// }
	SUBNET=${SUBNET// }
	NETADDR=${NETADDR// }
	NODECOUNT=$(($NODECOUNT-2)) # removing server host + broadcast
}

dnsmasq_conf()
{
# The following (commneted) would be much cleaner but this can't work in the livecd environment since the portage tree doesn't exist
# and the template file is thus non-existant (${FILESDIR}/...) .
#	cp ${FILESDIR}/dnsmasq-2.4-conf ${ROOT}/etc/dnsmasq.conf
#
#	for I in PUBETH LOCALDOMAINNAME TFTPROOT IPSTART IPSTOP SUBNET CLUSTER_ETH_IP PXELINUX HOSTNAME NFSROOT NODECOUNT DNSMASQSCRIPT
#	do
#		sed -i -e"s:%%$I:${!I}:g" ${ROOT}/etc/dnsmasq.conf
#	done
#	cp 

	cat > ${ROOT}/etc/dnsmasq.conf <<-EOF	
	# For the sake of non-duplication of documentation, please view the dnsmasq manpage for a description of these options.
	# Public interface
	except-interface=$PUBETH
	
	# We are the authoritative DHCP server for this network
	dhcp-authoritative

	# Set up the local-only domain
	local=/${LOCALDOMAINNAME}/127.0.0.1
	domain=$LOCALDOMAINNAME
	expand-hosts

	# We need the following to retain dhcp served resolution info:
	resolv-file=/etc/dnsmasq-resolv.conf

	dhcp-script=$DNSMASQSCRIPT

	# Some netiquette:
	domain-needed
	bogus-priv
	
	enable-tftp
	tftp-root=${TFTPROOT}

	# Send RFC-3397 DNS domain search DHCP option. WARNING: Your DHCP client
	# probably doesn't support this...... (option 119)
	dhcp-option=119,$LOCALDOMAINNAME
	# Domain DNS name
	dhcp-option=15,$LOCALDOMAINNAME

	# set the master node as the NTP server
	dhcp-option=option:ntp-server,0.0.0.0

	### Node definitions, this could be in a separate file
	# Turn on the DHCP server, pass out addresses from $IPSTART to $IPSTOP
	# with subnet mask of $SUBNET with a 12-hour lease
	dhcp-range=nodes,$IPSTART,$IPSTOP,$SUBNET,12h

	# Override the default route supplied by dnsmasq, which assumes the
	# router is the same machine as the one running dnsmasq.
	dhcp-option=3,$CLUSTER_ETH_IP

	# This tells the clients the hostname and IP of the TFTP server
	dhcp-boot=${PXELINUX},$(hostname),$CLUSTER_ETH_IP

	# address and root path of NFS server
	dhcp-option=17,$CLUSTER_ETH_IP:${NFSROOT}
	
	# The default is 150, which is quite low if one wants a cluster with more than 150 nodes.
	dhcp-lease-max=$NODECOUNT

	EOF
}

pxe_conf()
{
	PROFNAME="$(uname -m)_node"
	BOOTPATH="${NFSROOT##$TFTPROOT}"
	BOOTPATH="${BOOTPATH#/}/boot"

	mkdir -p ${TFTPROOT}/pxelinux.cfg

	cat > ${TFTPROOT}/pxelinux.cfg/default <<-EOF
	#prompt 1
	#timeout 50
	#say Press F1 for boot profiles, default is $PROFNAME in 5 seconds...
	#F1 BootProfiles
	default $PROFNAME

	label $PROFNAME
	  kernel ${BOOTPATH}/kernel
	#important: AuFS only supports NFSv3
	  append ip=dhcp nfsroot=${CLUSTER_ETH_IP}:${NFSROOT},hard,intr,nfsvers=3 init=/boot/stateless.sh unionmod=aufs rwdev=tmpfs

	label local
	  localboot 0
	EOF

	cat > ${TFTPROOT}/BootProfiles <<-EOF
	
	Type $PROFNAME for regular bootup (default after 5 seconds)
	Type local to boot on the local HDD
	EOF

	ln -s "${ROOT}/usr/lib/syslinux/pxelinux.0" "${TFTPROOT}/pxelinux.0"
}

# In: PATH_TO_EXPORT ro/rw fsid
# out: /etc/exports (and ${NFSROOT}/etc/autofs/auto.nfs
add_exports()
{
	local I="$1"
	local MODE=$2
	local fsid=$3

	if use nonfsv4 ; then
		echo "${I} $NETADDR/$SUBNET($MODE,$NFSEPORTOPTS,fsid=$fsid)" >> ${ROOT}/etc/exports
	else
		mkdir -p ${NFSROOT}/${I}
		mount -o bind ${I} ${NFSROOT}/${I}
	fi
	use autofs && echo "${I/\/} -$MODE,$NFSMOUNTOPTS $CLUSTER_ETH_Ir:${I}" >> ${NFSROOT}/etc/autofs/auto.nfs
}

nfs_exports_conf()
{
	local fsid=0

	echo "${NFSROOT} $NETADDR/$SUBNET(ro,$NFSEPORTOPTS,fsid=0)" > ${ROOT}/etc/exports

	# Yes, this could be looped on RO and RW...but that wouldn't be any more redable than it already isn't ;)
	for I in $ROEXPORTS
	do
		((fsid++))
		add_exports "$I" "ro" $fsid
	done

	for I in $RWEXPORTS
	do
		((fsid++))
		add_exports "$I" "rw" $fsid
	done

}

resolv_conf()
{
	cat > /etc/resolvconf/resolv.conf.d/base <<-EOF
	search $LOCALDOMAINNAME
	nameserver 127.0.0.1
	EOF
}

parse_config()
{
	[[ -f ${1} ]] || die "${1} missing!!! re-emerge ${PF}"
	. ${1}
	[[ $CONFIG_OK != "yes" ]] && die "You need to edit ${CONFIG_FILE} and set CONFIG_OK=\"yes\""
}

pkg_setup() {
	if ! built_with_use net-dns/dnsmasq tftp ; then
		die "net-dns/dnsmasq must be built with USE=tftp to be used with ${P}!"
	fi
	use autofs && die "We're not there yet ;)"
}

src_unpack(){
	mkdir -p ${S}

	cp ${FILESDIR}/node-manager ${S}/node-manager || die "Som ting Wong"
	sed -e "s:%%MODULESPATH:$MODULESPATH:" -i ${S}/node-manager 
	if use beep ; then
		sed -e "s:%%BEEPS:true:" -i ${S}/node-manager
	else
		sed -e "s:%%BEEPS:false:" -i ${S}/node-manager
	fi

	if use pbs ; then
		cp ${FILESDIR}/torque-* ${S}/
		sed -e "s:%%CONFPATH/%%CONFIG_FILE:$CONFPATH/$CONFIG_FILE:" -i ${S}/torque-add
	fi

}

src_install(){
	dodir   ${CONFPATH}
	insinto ${CONFPATH}
	doins   ${FILESDIR}/${CONFIG_FILE}
	
	dosbin ${S}/node-manager
	dosbin ${FILESDIR}/setup-pwdless-ssh

	# add and delnode are derived from the dnsmasq add/del/old directives
	# passed onto the node-manager script...don't change arbritrarily
	dodir   ${MODULESPATH}
	exeinto ${MODULESPATH}
	doexe   ${FILESDIR}/c3-* 
	use pbs && doexe ${S}/torque-* 

	elog "You need to edit ${CONFPATH}/${CONFIG_FILE} to your likings"
	elog "Once you are done, set CONFIG_OK=\"yes\" in there and"
	elog "call emerge --config =${CATEGORY}/${PF}"
}

pkg_config()
{
	parse_config "${CONFPATH}/${CONFIG_FILE}"

	[ -d ${NFSROOT} ] || die "${NFSROOT} is missing! Either create your own of find one ;)"
	if [ ! -f ${NFSROOT}/boot/kernel ]; then
		ewarn "Couldn't find ${NFSROOT}/boot/kernel !"
		ewarn "This is what we built the config files to expect."
		ewarn "You might have to perform a catalyst build to generate the NFS root filesystem."
	fi
	
	get_ip_config
	dnsmasq_conf
	resolv_conf
	pxe_conf
	nfs_exports_conf
	
	if use pbs; then
		TORQUED="pbs_server pbs_sched"
		# we don't want to call --config in a catalyst build
		if ! use livecd ; then
			emerge --config torque
			ROOT==$NFSROOT emerge --config torque
		fi
	fi

	for I in dnsmasq netmount nfs $TORQUED
	do
		rc-update add $I default
		/etc/init.d/$I start
	done
	
	elog "Since we just changed a resolvconf file, "
	elog "it is required that your main NIC interface be restarted"
	elog "or at least a renewal of dhcp be triggered."
	elog 'ie: with dhcpcd one calls `dhcpcd -n eth0`'
	elog "You will then want to restart dnsmasq."
}

