# broken with GCC 4.3 (RPM pulls in...let's see if we can do without it)
<=sys-libs/db-3.2.9-r11
# Anything smaller than dev-libs/boost-1.35.0 won't compile with gcc-4.3
# This means no OpenOffice-2.4.0 since it requires boost-1.33.1...
<=dev-libs/boost-1.35.0
# Anything less than =media-libs/musicbrainz-3.0.1 won't compile with GCC-4.3
<=media-libs/musicbrainz-3.0.1
