umount /var/git/clustering-livecd/catalyst/livecd/2008.0/overlay/tftproot/nfsroot/x86_64/
umount /var/tmp/catalyst/tmp/default

clean(){
	find /var/tmp/catalyst/packages/ -regextype posix-extended -regex '.*(beowulf|aufs|ldap-auth|torque|diradm).*' -exec rm -f {} \;
}

run_catalyst(){
	schedtool -B -n -4 -e catalyst -f $1 || exit 1
}

clean

mount -t tmpfs -o size=7G,noatime catalystbuild /var/tmp/catalyst/tmp/default

run_catalyst /var/git/clustering-livecd/catalyst/nfsroot/2008.0/specs/amd64/nfsroot_stage4-slim.spec
run_catalyst /var/git/clustering-livecd/catalyst/livecd/2008.0/specs/amd64/livecd-stage1-minimal.spec

# untill catalyst allows inserting our own MOTD even with the gentoo-release-livecd livecd/type,
# we overwrite the installed MOTD from catalyst:
cp /var/git/clustering-livecd/catalyst/livecd/2008.0/misc/clusterlivecd.motd.txt /usr/lib64/catalyst/livecd/files/livecd.motd.txt

mount -o bind /var/tmp/catalyst/tmp/default/stage4-amd64-nfsroot-20080816/ /var/git/clustering-livecd/catalyst/livecd/2008.0/overlay/tftproot/nfsroot/x86_64/

clean

run_catalyst /var/git/clustering-livecd/catalyst/livecd/2008.0/specs/amd64/livecd-stage2-minimal.spec

