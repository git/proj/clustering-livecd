#!/bin/bash

if [[ $# != 1 ]]; then
	echo "Specify snapshot to switch to, ie: 
$0 20080806"
	exit 1
fi

for I in catalyst/livecd/2008.0/specs/amd64 catalyst/nfsroot/2008.0/specs/amd64
do
	sed -i -e "s#snapshot:.*#snapshot: $1#" ${I}/*.spec
done

