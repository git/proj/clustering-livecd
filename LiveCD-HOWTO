# Eric Thibodeau
# Initial 01 Jul 2008
# Updated 17 Aug 2008
# Google Summer of Code 2008: Gentoo Clustering LiveCD

## The build environment ##

If you want to be able to create liveCDs with as little hassle as possible you will need the following:

	- The loop module, it's required for the licecd-stage2 creation of the kernel
		modprobe loop

	- dev-util/subversion
	- dev-util/git
    - dev-util/catalyst (am using CVS version which is close to 2.0.6)
        ** for some reason, catalyst won't create the skeleton dirs so here is the sequence to get you started

        emerge dev-util/catalyst
        mkdir -p /var/tmp/catalyst/ /var/tmp/catalyst/snapshots /var/tmp/catalyst/tmp/default
		# edit /etc/catalyst/catalystrc and add relevant options such as:
		export MAKEOPTS="-j6"
		export FEATURES="parallel-fetch"
		# You haev to set this here if you want to enable the pbs (torque) use flag for the clustering liveCD
		# the server's name is required at compile time (unfortunately) and this is the only way of overriding
		# the build host as becoming the one defined as the server.
		# This also FORCES the user to use this name as the master node...yerk, I know...
		export PBS_SERVER_NAME="master.gentoo.local"
		# you can set CFLAGS here but it's not recommended
		# NOTE, don't try to use distcc, it's broken, the distcc user is not being created in the catalyst build

# releng is no longer required
##    - http://sources.gentoo.org/viewcvs.py/releng/ pulled in as /var/svnroot/releng
##        mkdir -p /var/svnroot/
##        cd /var/svnroot/
##        svn co svn://anonsvn.gentoo.org/releng

    - A recent snapshot (last one used was portage-20080806.tar.bz2), you have to modify the catalyst .spec files accordingly
        So get a snapshot and place it in /var/tmp/catalyst/snapshots (we are doing something evil here...):

        wget ftp://mirrors.tera-byte.com/pub/gentoo/snapshots/portage-20080806.tar.bz2 -O /var/tmp/catalyst/snapshots/portage-20080806.tar.bz2

*NOTE*: You have to modify the spec files accordingly (so they point to the correct portage snapshot). There is a _crude_ script that automates
this within the root of the repository and is used as such:

./switchshapshot.sh 20081014

In this example, all specs will use a snapshot from 20081014 (which you have to download into "/var/tmp/catalyst/snapshots"

	- A recent stage3, this eliminates many problems related to circular dependancies and collisions. We used drobbin's packages from funtoo.org
		mkdir -p /var/tmp/catalyst/builds/drobbins/
		cd /var/tmp/catalyst/builds/drobbins/
		wget http://www.funtoo.org/linux/amd64/funtoo-amd64-2008.06.27/stage3-amd64-2008.06.27.tar.bz2 

*NOTE* Again, changing seed source for the ebuilds implies modification to the stage1 spec files. No script this time, do it by editing the 
source_subpath parameter in two following spec files:

 ./catalyst/nfsroot/2008.0/specs/amd64/stage1.spec
 ./catalyst/livecd/2008.0/specs/amd64/stage1.spec

    - This git checked out into /var/git/:

        mkdir -p /var/git/
        cd /var/git/
        git clone git://git.overlays.gentoo.org/proj/clustering-livecd.git

	- We also need (some) overlays:
		# NOTE!!! I set up my layman to put all files into /usr/local/portage/ (and not de thefault of /usr/portage/local)
		# If you don't like this, setup the .spec files accrodingly
		layman -a sunrise
		layman -a science

## Before you can build the LiveCD/DVD, you have to build the nfsroot which is to be included in the iso...so skip to the NFSroot-HOWTO and come back here when you're done ;)
	
## Actually building the CD ##

** NOTE: if you have lots of RAM: **
	You can build everything in RAM with no problems as long as tmpfs has at least 6Gigs (for the LiveCD stages)
	alias mnttmp='mount -t tmpfs -o size=6G,noatime catalystbuild /var/tmp/catalyst/tmp/default'
**

	# Barring the bugs 229103 and 206753, the following sequence should get you down to a LiveCD (haha...HAHA...HAHAHAHAHA)
	# These sequences are the ones I put in a script to whipe the slate clean and rebuild the environment 
	# Well, almost, I don't delete the packages dir except for some packages for which I often want to force a rebuild.

	# Stages 1 to 3
	cd clustering-livecd/catalyst/releases/2008.0/specs/amd64/
	for I in stage*
	do 
		mnttmp
		catalyst -f $I
		# we empty out stuff we don't need anymore ;)
		umount /var/tmp/catalyst/tmp/default
	done

	# Minimal CD:
	umount /var/git/clustering-livecd/catalyst/livecd/2008.0/overlay/tftproot/nfsroot/x86_64/
	umount /var/tmp/catalyst/tmp/default
	
	find /var/tmp/catalyst/packages/ -regextype posix-extended -regex '.*(beowulf|aufs|ldap-auth).*' -exec rm -f {} \;
	
	mount -t tmpfs -o size=7G,noatime catalystbuild /var/tmp/catalyst/tmp/default
	schedtool -B -n -4 -e catalyst -f /var/git/clustering-livecd/catalyst/nfsroot/2008.0/specs/amd64/nfsroot_stage4.spec
	schedtool -B -n -4 -e catalyst -f /var/git/clustering-livecd/catalyst/livecd/2008.0/specs/amd64/livecd-stage1-minimal.spec
	mount -o bind /var/tmp/catalyst/tmp/default/stage4-amd64-nfsroot-20080806/ /var/git/clustering-livecd/catalyst/livecd/2008.0/overlay/tftproot/nfsroot/x86_64/
	
	find /var/tmp/catalyst/packages/ -regextype posix-extended -regex '.*(beowulf|aufs|ldap-auth).*' -exec rm -f {} \;
	schedtool -B -n -4 -e catalyst -f /var/git/clustering-livecd/catalyst/livecd/2008.0/specs/amd64/livecd-stage2-minimal.spec


	# "Full" CD:
	Same as above but replace livecd-stage?-minimal.spec with livecd-stage?.spec
	
	# At the end, an iso image should be waiting for you as /tmp/livecd-amd64-installer-2008.0.iso ;)


